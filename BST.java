// Binary Search Tree
public class BST {
Node root;
private class Node
	{
	private Node left;
	private Node right;
	private String data;
	Node(String k)
		{
		left = right = null;
		data = k;
		}
	}

	public int insert(String k)
	{
			Node n = new Node(k);
			return insert(root,n);
	}

	private int insert(Node r, Node n)
	{
		if(root == null)
			root = n;
		else{
			if((r.data).compareTo( n.data)>0) 
			{
				if(r.left == null) 
				{
					r.left = n;
				}
				else 
					insert(r.left,n); 
			}
			if((r.data).compareTo( n.data)<0)
			{ 
				if(r.right == null) 
				{
					r.right = n;
				}
				else 
					insert(r.right,n);
			}
			if((r.data).compareTo(n.data)==0) {
				System.out.print("This element is already exist in tree:");
				System.out.print(n.data+"\n");
                return 1;
			}
			
		}
		return 0;
	}
	
	public int search(String k)
		{
		int control = search(root, k);
		//System.out.println(control);
		if(control==1) {
			
			return 1;
			}
		else {
		
		return 0;
		}
	}
	
	private int search(Node r, String k)
	{
		if( r != null)
		{
			if(r.data.compareTo(k)==0) {
				//System.out.println("EQUALS"+r.data+ "DATA :" +k);
				
				return 1;}
			if((r.data).compareTo(k)>0) {
				//System.out.println("LEFT"+r.data+ "DATA :" +k);
				int control= search(r.left,k);
				return control;
				}
			if((r.data).compareTo(k)<0) {
				//System.out.println("RIGHT :"+r.data+ "DATA :" +k);
				int control= search(r.right,k);
				return control;}
		}
	
	return 0;
	}
	
	public void max()
	{
		System.out.println(max(root));
	}	
	private String max(Node r)
	{
		if(r!=null)
		{
			max(r.right);
		}
		return r.data;
	}
	
	public void min()
	{
		System.out.println(min(root));
	}	
	private String min(Node r)
	{
		if(r!=null)
		{
			min(r.left);
		}
		return r.data;
	}
	
	public void print(Node r)
	{
		if( r != null )
		{
			print(r.left);
			System.out.print(r.data + " ");
			print(r.right);
		}
	}
	}