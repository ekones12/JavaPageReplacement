import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Assignment2 {
	
	

	public static void reader (String args1 ) {
		
		
		int memorysize = 0;
		BufferedReader br = null;
        String line = "";
        String cvsSplitBy = " ";
        FileWriter writer  = null;
        String NEW_LINE_SEPARATOR = "\n";
     try {
        	writer = new FileWriter("output.txt");
            br = new BufferedReader(new FileReader(args1));
            for (int lines=0; lines<2 &&(line = br.readLine()) != null;lines++) {
                String[] datas = line.split(cvsSplitBy); 
                if(lines == 0) {
                	 memorysize=  Integer.parseInt(datas[1]);
                	 writer.append("Memory".toString());
                	 writer.append(cvsSplitBy);
                	 writer.append(String.valueOf(memorysize));
                	 writer.append(NEW_LINE_SEPARATOR); 
                }
                if(lines == 1) {
                	line = br.readLine();
                	if(datas[1].equals("FIFO")) {
                		 writer.append("FIFO Page Replacement".toString());
                		 writer.append(NEW_LINE_SEPARATOR);
                		 writer.append("Binary Search Tree".toString());
                	
                		 writer.append(NEW_LINE_SEPARATOR);
                		 
                	int controltimes=0;
                	CircularQueue cq = new CircularQueue(memorysize);
                	long first =  System.currentTimeMillis();
                	 while ((line = br.readLine()) != null) {
                         String[] datasfifo = line.split(cvsSplitBy);
                         BST tree = new BST();
                         for(int j=0;j <(cq.queue.length);j++) {
                        	 if(cq.queue[j]!= null) {
                        	 tree.insert(cq.queue[j]);
                        	 //System.out.println(cq.queue[j]);
                        	 }
                         }
                        // tree.print(tree.root);
                         int control = tree.search(datasfifo[1]);
                         //System.out.println("control olustuktan sonrasi : "+control);
                         if(control ==0) {
                        	cq.add(datasfifo[1]);
                         	writer.append("Page Fault	 ".toString());
                         	controltimes++;
                         }if(control ==1) {
                        	 writer.append("			 ".toString());
                         }
                        
                         for(int a=0;a<cq.queue.length;a++) {
                        	 if(cq.queue[a]!= null) {
                        		 //System.out.println(cq.queue[a]);
                        		 writer.append(cq.queue[a].toString());
                        		 writer.append(cvsSplitBy);
                        			 
                        		 }
                        	
                        }
                         writer.append(NEW_LINE_SEPARATOR);
                         
                	 }
                	 long last =  System.currentTimeMillis()-first;
                	 writer.append(String.valueOf(controltimes));
                	 writer.append("\nThe algorithm time of FIFO Page Replacement : ");
                	 
                	 writer.append(String.valueOf(last));
                	 writer.append(" milisecond.");
                	 //System.out.println(last);
                	}
                	
                	
                	
                	if(datas[1].equals("SecondChance")) {
                		
                		 writer.append("SecondChance Page Replacement".toString());
                		 writer.append(NEW_LINE_SEPARATOR);
                		 writer.append("Binary Search Tree".toString());
                		 writer.append(NEW_LINE_SEPARATOR);
                		 
                	int controltimes=0;
                	CircularQueue cq = new CircularQueue(memorysize);
                	 long first =  System.currentTimeMillis();
                	 while ((line = br.readLine()) != null) {
                         String[] datassecondchange = line.split(cvsSplitBy);
                         BST tree = new BST();
                         for(int j=0;j <(cq.queue.length);j++) {
                        	 if(cq.queue[j]!= null) {
                        	 tree.insert(cq.queue[j]);
                        	 //System.out.println(cq.queue[j]);
                        	 }
                         }
                        // tree.print(tree.root);
                         int control = tree.search(datassecondchange[1]);
                         int rep=0;
                         //System.out.println("control olustuktan sonrasi : "+control);
                         if(control ==0) {
                        	 
                        	rep=cq.add(datassecondchange[1]);
                         	writer.append("Page Fault	 ".toString());
                         	controltimes++;
                         }if(control ==1) {
                        	 for(int j=0;j <(cq.queue.length);j++) {
                            	 if(cq.queue[j]!= null) {
                            		 if(cq.queue[j].compareTo(datassecondchange[1])==0) {
                            			// System.out.println(j);
                            			 cq.idqueue[j]=1;
                            			 //System.out.println(cq.queue[j]);
                            	 }}
                             }
                        	 writer.append("			 ".toString());
                         }
                        
                         for(int a=0;a<cq.queue.length;a++) {
                        	 if(cq.queue[a]!= null) {
                        		 //System.out.println(cq.queue[a]);
                        		 writer.append(cq.queue[a].toString());
                        		 writer.append(cvsSplitBy);
                        			 
                        		 }
                        	
                        }
                         	if(control ==0) {
                         		if(rep>0)
                         			writer.append("Second Chance ".toString());
                        	 	for(int k=rep;k>0;k--) {
                        	 		writer.append(cq.queue[(cq.rear+cq.size-(k+1))%cq.size].toString());
                        	 		writer.append(cvsSplitBy);}
                        	 //System.out.println(rep);
                     
                         }
                         writer.append(NEW_LINE_SEPARATOR);
                         
                	 }
                	 long last =  System.currentTimeMillis()-first;
                	 writer.append(String.valueOf(controltimes));
                	 writer.append("\nThe algorithm time of SecondChance Page Replacement : ");
                	 writer.append(String.valueOf(last));
                	 writer.append(" milisecond.");
                	// System.out.println(last);
					}
                	
                	
                	
                	if(datas[1].equals("PriorityQueue")) {
                		
               		 writer.append("PriorityQueue Page Replacement".toString());
               		 writer.append(NEW_LINE_SEPARATOR);
               		 writer.append("Binary Search Tree".toString());
               	
               		 writer.append(NEW_LINE_SEPARATOR);
               		 
	               	 int controltimes=0;
	               	 CircularQueue cq = new CircularQueue(memorysize);
	               	long first =  System.currentTimeMillis();
	               	 while ((line = br.readLine()) != null) {
	                        String[] datasheap = line.split(cvsSplitBy);
	                        BST tree = new BST();
	                        for(int j=0;j <(cq.queue.length);j++) if(cq.queue[j]!= null)  tree.insert(cq.queue[j]);
	                        
	                        //tree.print(tree.root);
	                        //System.out.println("\n");
	                        int control = tree.search(datasheap[1]);
	                        //System.out.println("control olustuktan sonrasi : "+control);
	                        if(control ==0) {
	                        	int cont=0;
	                        	MaxPQ<String> test = new MaxPQ<String>(memorysize);
	                        	 for(int j=0;j <(cq.queue.length);j++) {
	                        		 if(cq.queue[j]!= null) { 
	                        			 test.insert(cq.queue[j]);
	                        			 cont++;
	                        		 }
	                        		 
	                        	 }
	                        	 if(cont==memorysize) {
	                        	 //System.out.println(test.max()+" zzz-zzz-zzz");
	                        	 for(int j=0;j <(cq.queue.length);j++) {
	                        		 if(cq.queue[j].equals(test.max()))
	                        			 cq.queue[j]=datasheap[1];
	                        	 }
	                        	 
	                        	 }
	                        	 
	                        	 
	                        	 else	
	                        		 cq.add(datasheap[1]);
	                        	writer.append("Page Fault	 ".toString());
	                        	controltimes++;
	                        }if(control ==1) {
	                       	 writer.append("			 ".toString());
	                        }
	                       
	                        for(int a=0;a<cq.queue.length;a++) {
	                       	 if(cq.queue[a]!= null) {
	                       		 //System.out.println(cq.queue[a]);
	                       		 writer.append(cq.queue[a].toString());
	                       		 writer.append(cvsSplitBy);
	                       			 
	                       		 }
	                       	
	                       }
	                        writer.append(NEW_LINE_SEPARATOR);
	                        
	               	 }
	               	 long last =  System.currentTimeMillis()-first;
	               	 writer.append(String.valueOf(controltimes));
	               	 writer.append("\nThe algorithm time of PriorityQueue Page Replacement : ");
                	 writer.append(String.valueOf(last));
                	 writer.append(" milisecond.");
                	 //System.out.println(last);
	               	}
	               	
	               	//--------son----
               }
               
            }
           

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
	        	
            	writer.flush();

                writer.close();

            } catch (IOException e) {

                System.out.println("Error while flushing/closing fileWriter !!!");

                e.printStackTrace();

            }
        }
}
	
	
	
	
	public static void main(String[] args) {
		
		reader(args[0]);//FIFO
		
		//reader(args[1]);//	second change 
		//reader(args[2]);// 	priortyqueue
	}

}
